﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Akka.Actor;
using Akka.Configuration;
using Akka.Remote;

namespace TJSettingsManagement
{
  public class ManagerOfActorSystem
  {
    public string NameOfActorSystem { get; } = "TJSettings";

    public string DbFileName { get; private set; } = string.Empty;

    public string NameOfActorCommander { get; private set; } = string.Empty;

    public string PasswordOfActorCommander { get; private set; } = string.Empty; // TODO: It is not used now.

    public string ActorSystemIpAddress { get; private set; } = string.Empty;

    public int ActorSystemTcpPort { get; private set; } = 0;

    public ActorSystem AcSystem { get; private set; } = null;

    public IActorRef AxCommander { get; private set; } = null;

    public IActorResponseReceiver ActorResponseReceiver { get; private set; } = null;

    private ManagerOfActorSystem() { }

    public static ManagerOfActorSystem Create() => new ManagerOfActorSystem();

    public string GetActorSystemConfig()
    {
      return
        "Commander name = " + AxCommander.Path.ToStringWithAddress() + "\r\n" +
        "Actor system config = " + AcSystem.Settings.Config.ToString();
    }

    public void Configure(IActorResponseReceiver receiver) => ActorResponseReceiver = receiver;

    private Config GetConfig(string ipAddress, int tcpPort)
    {
      string StringConfig = @"
      akka {
          actor {
              provider = remote
          }
          remote {
              enabled-transports = [""akka.remote.dot-netty.tcp""]
              dot-netty.tcp {
                  maximum-frame-size = 4000000b
                  port = " + tcpPort.ToString() + @"
                  hostname = " + ipAddress + @"
              }
          }
      }";

      //Trace.WriteLine(StringConfig);
      return ConfigurationFactory.ParseString(StringConfig);
    }

    public Exception StartActorSystem(string dbFileName, string ipAddress, int tcpPort, string nameCommander, string passwordCommander = "")
    {
      Exception error = null;
      ActorSystemIpAddress = ipAddress;
      ActorSystemTcpPort = tcpPort;
      NameOfActorCommander = nameCommander;
      PasswordOfActorCommander = passwordCommander;
      DbFileName = dbFileName;
      try
      {
        var config = GetConfig(ActorSystemIpAddress, ActorSystemTcpPort);
        AcSystem = ActorSystem.Create(NameOfActorSystem, config);
        AxCommander = AcSystem.ActorOf<AcCommander>(nameCommander);
        AxCommander.Tell(this);
      }
      catch (Exception ex)
      {
        error = ex;
      }
  
      if (error == null)
      {
        DetectTcpPortOfActorSystem();
        AxCommander.Tell(AskOpenDatabase.Create(dbFileName, string.Empty));       
      }

      return error;
    }

    public void DetectTcpPortOfActorSystem()
    {
      if (ActorSystemTcpPort != 0) return;
      AskActorSystemTcpPortNumber request = AskActorSystemTcpPortNumber.Create
        (
          AcSystem,
          ActorSystemIpAddress,
          NameOfActorCommander
        );     
      AxCommander.Tell(request);
    }
  }
}
