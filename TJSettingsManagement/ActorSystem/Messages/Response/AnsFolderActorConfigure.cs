﻿using System.Collections.Generic;
using TJSettings;

namespace TJSettingsManagement
{
  public class AnsFolderActorConfigure
  {
    public List<Folder> Folders { get; } = null;

    public AnsFolderActorConfigure(List<Folder> list)
    {
      Folders = list;
    }

    public static AnsFolderActorConfigure Create(List<Folder> list) => new AnsFolderActorConfigure(list);
  }
}