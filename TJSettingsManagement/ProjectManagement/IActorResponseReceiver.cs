﻿using TJStandard;

namespace TJSettingsManagement
{
  public interface IActorResponseReceiver
  {
    void ReportActorSystemTcpPort(int PortNumber);
    void ReportDatabaseConnection(AskOpenDatabase request, ReturnCode code);
    void ReportTvRootNode(AnsTvRootNode arg);
    void ReportFolderInfo(AnsFolderInfo arg);
  }
}
