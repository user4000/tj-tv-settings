﻿namespace TJSettingsManagement
{
  public class AnsActorSystemTcpPortNumber
  {
    public int PortNumber { get; } = 0;

    public string RemotePath { get; } = string.Empty;

    public AnsActorSystemTcpPortNumber(int portNumber, string path)
    {
      PortNumber = portNumber;
      RemotePath = path;
    }

    public static AnsActorSystemTcpPortNumber Create(int portNumber, string path) => new AnsActorSystemTcpPortNumber(portNumber, path);

  }
}