﻿namespace TJSettingsManagement
{
  public class AskFolderInfo
  {
    public int IdFolder { get; } = -1;

    public string FolderPath { get; } = string.Empty;

    public AskFolderInfo(int idFolder, string folderPath)
    {
      IdFolder = idFolder;
      FolderPath = folderPath;
    }

    public static AskFolderInfo Create(int idFolder, string folderPath = "") => new AskFolderInfo(idFolder, folderPath);

    public static AskFolderInfo Create(string folderPath) => new AskFolderInfo(-1, folderPath);

  }
}