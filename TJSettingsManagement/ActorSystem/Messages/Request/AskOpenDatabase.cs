﻿namespace TJSettingsManagement
{
  public class AskOpenDatabase : AskMessage
  {
    public string FileName { get; } = string.Empty;

    public string ConnectionConfig { get; } = string.Empty;

    public AskOpenDatabase(string fileName, string connectionConfig)
    {
      FileName = fileName;
      ConnectionConfig = connectionConfig;
    }

    public static AskOpenDatabase Create(string fileName, string connectionConfig) => new AskOpenDatabase(fileName, connectionConfig);
  }
}