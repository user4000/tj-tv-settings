﻿using Telerik.WinControls.UI;

namespace TJSettings
{
  public interface ISendRequestToDatabase
  {
    void RequestLoadData();
    void RequestTreeviewSelectedNodeChanged(object sender, RadTreeViewEventArgs e);
  }
}

