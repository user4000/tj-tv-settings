﻿using System;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using TJStandard;
using TJSettings;
using TJSettingsFormTreeView;
using TJFramework;
using static TJFramework.TJFrameworkManager;

namespace TJAdminSettings
{
  public class ProjectManager : IOutputMessage //, ISendRequestToDatabase
  {
    public const string Empty = "";

    public ManagerFormTreeView MnFormTreeView { get; internal set; } = null;

    public void OutputMessage(string message, string header = Empty)
    {
      Ms.Message(message, header).NoAlert().Debug();
    }

    internal void EventPageChanged(string name)
    {

    }

    internal void EventAfterAllFormsAreCreated()
    {

    }

    internal void EventBeforeMainFormClose()
    {

    }

    public void RequestLoadData()
    {
      
    }
  }
}