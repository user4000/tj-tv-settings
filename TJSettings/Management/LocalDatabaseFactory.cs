﻿namespace TJSettings
{
  public class LocalDatabaseFactory // This is singleton //
  {
    private static readonly object padlock = new object();
    private static LocalDatabaseOfSettings Database { get; set; } = null;
    public static LocalDatabaseOfSettings Create()
    {
      if (Database == null)
      {
        lock (padlock)
        {
          if (Database == null)
          {
            Database = new LocalDatabaseOfSettings();
          }
        }
      }
      return Database;
    }
  }
}

