﻿using System.Collections.Generic;
using TJSettings;

namespace TJSettingsManagement
{
  public class AnsFolderInfo
  {
    public Folder Folder { get; } = null;

    public List<Folder> Folders { get; } = null;

    public AnsFolderInfo(Folder folder, List<Folder> folders)
    {
      Folder = folder;
      Folders = folders;
    }

    public static AnsFolderInfo Create(Folder folder, List<Folder> folders) => new AnsFolderInfo(folder, folders);
  }
}