﻿using System;

namespace TJSettingsManagement
{
  public abstract class AskMessage
  {
    public string IdRequest { get; } = Ulid.NewUlid().ToString();
  }
}