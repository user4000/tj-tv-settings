﻿namespace TJStandard
{
  public class ReturnCodeFormatter
  {
    public static char DoubleQuote { get; } = '"';

    public static bool MayBeReturnCode(string json) =>
      json.Contains(DoubleQuote + nameof(ReturnCode.Number)    + DoubleQuote) &&
      json.Contains(DoubleQuote + nameof(ReturnCode.Message)   + DoubleQuote) &&
      json.Contains(DoubleQuote + nameof(ReturnCode.IdObject)  + DoubleQuote) &&
      json.Contains(DoubleQuote + nameof(ReturnCode.Note)      + DoubleQuote);
    
    public static string ToString(ReturnCode code)
    {
      return $"{code.Number};{code.IdObject};{code.Message};{code.Note}";
    }
  }
}
