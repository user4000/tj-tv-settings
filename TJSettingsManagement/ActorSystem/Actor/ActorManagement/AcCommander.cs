﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Akka.Actor;
using TJSettings;
using TJStandard;

namespace TJSettingsManagement
{
  public class AcCommander : ReceiveActor
  {
    IActorRef AxDbOperator { get; set; }

    IActorRef AxTcpPortDetector { get; set; }

    ManagerOfActorSystem Manager { get; set; }

    IActorResponseReceiver User { get; set; } 

    LocalDatabaseOfSettings DbOperator { get; set; }

    IOutputMessage MessageReceiver { get; set; }

    int PortNumber { get; set; } = 0;

    void CreateChildActors()
    {
      AxDbOperator = Context.ActorOf<AcDbOperator>(nameof(AcDbOperator));
      AxTcpPortDetector = Context.ActorOf<AcTcpPortDetector>(nameof(AcTcpPortDetector));
    }

    public AcCommander()
    {
      Receive<AskOpenDatabase>(arg => EventOpenDatabase(arg));     
      Receive<AskCheckTcpPortNumber>(arg => EventSearchingTcpPortNumber(arg));

      Receive<AskActorSystemTcpPortNumber>(arg => EventRequestActorSystemTcpPortNumber(arg));
      Receive<AnsActorSystemTcpPortNumber>(arg => EventResponseActorSystemTcpPort(arg));

      Receive<AskTvRootNode>(arg => AxDbOperator.Tell(arg));
      Receive<AnsTvRootNode>(arg => User.ReportTvRootNode(arg));

      Receive<AskFolderInfo>(arg => AxDbOperator.Tell(arg));
      Receive<AnsFolderInfo>(arg => User.ReportFolderInfo(arg));

      Receive<ManagerOfActorSystem>(arg => EventManagerOfActorSystem(arg));
      Receive<IOutputMessage>(arg => EventInitOutputMessageObject(arg));

      Receive<string>(arg => EventString(arg));

      CreateChildActors();
    }

    private void EventManagerOfActorSystem(ManagerOfActorSystem arg)
    {
      Manager = arg;
      User = Manager.ActorResponseReceiver;
    }

    void EventSearchingTcpPortNumber(AskCheckTcpPortNumber arg)
    {
      Sender.Tell(arg);    
    }

    void EventRequestActorSystemTcpPortNumber(AskActorSystemTcpPortNumber arg)
    {
      AxTcpPortDetector.Tell(arg);
    }

    void EventResponseActorSystemTcpPort(AnsActorSystemTcpPortNumber arg)
    {
      if ((PortNumber == 0) && (arg.PortNumber > 0)) PortNumber = arg.PortNumber;
      Manager.ActorResponseReceiver.ReportActorSystemTcpPort(PortNumber);
    }

    void EventInitOutputMessageObject(IOutputMessage arg) => MessageReceiver = arg;

    void EventString(string arg)
    {
      if (MessageReceiver != null) MessageReceiver.OutputMessage(Self.Path + ": received a message=" + arg, "Actor Commander reports");
    }

    void EventOpenDatabase(AskOpenDatabase arg)
    {   
      DbOperator = LocalDatabaseFactory.Create();
      ReturnCode code = DbOperator.ConnectToDatabase(arg.FileName, arg.ConnectionConfig);
      if (code.Success) AxDbOperator.Tell(DbOperator);
      Sender.Tell(AnsMessage.Create(arg.IdRequest, code));
      Manager.ActorResponseReceiver.ReportDatabaseConnection(arg, code);
    }
  }
}