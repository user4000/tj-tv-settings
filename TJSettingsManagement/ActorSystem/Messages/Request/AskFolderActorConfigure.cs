﻿using Akka.Actor;
using TJSettings;

namespace TJSettingsManagement
{
  public class AskFolderActorConfigure
  {
    public IActorRef DbOperator { get; } = null;

    public Folder Folder { get; } = null;

    public AskFolderActorConfigure(IActorRef dbOperator, Folder folder)
    {
      DbOperator = dbOperator;
      Folder = folder;
    }

    public static AskFolderActorConfigure Create(IActorRef dbOperator, Folder folder) => new AskFolderActorConfigure(dbOperator, folder);
  }
}