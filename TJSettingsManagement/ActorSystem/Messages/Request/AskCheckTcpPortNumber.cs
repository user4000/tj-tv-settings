﻿namespace TJSettingsManagement
{
  public class AskCheckTcpPortNumber
  {
    public int PortNumber { get; } = 0;

    public string RemotePath { get; } = string.Empty;

    public AskCheckTcpPortNumber(int portNumber, string path)
    {
      PortNumber = portNumber;
      RemotePath = path;
    }

    public static AskCheckTcpPortNumber Create(int portNumber, string path) => new AskCheckTcpPortNumber(portNumber, path);

  }
}