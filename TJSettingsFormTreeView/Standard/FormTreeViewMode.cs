﻿namespace TJSettingsFormTreeView
{
  public enum FormTreeViewMode
  {
    OneLocalSettingsDatabase = 1,
    SettingsDatabaseAdministrator = 2
  }
}
