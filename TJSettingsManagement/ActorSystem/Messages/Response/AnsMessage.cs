﻿using TJStandard;

namespace TJSettingsManagement
{
  public class AnsMessage
  {
    public ReturnCode Code { get; }

    public string IdRequest { get; }

    public AnsMessage(string idRequest, ReturnCode code)
    {
      IdRequest = idRequest;
      Code = code;
    }

    public static AnsMessage Create(string idRequest, ReturnCode code) => new AnsMessage(idRequest, code);
  }
}