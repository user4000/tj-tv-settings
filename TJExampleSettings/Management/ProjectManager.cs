﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Akka.Actor;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using TJFramework;
using TJSettings;
using TJSettingsFormTreeView;
using TJSettingsManagement;
using TJStandard;
using static TJExampleSettings.Program;
using static TJFramework.TJFrameworkManager;

namespace TJExampleSettings
{
  public class ProjectManager : IOutputMessage, IActorResponseReceiver, ISendRequestToDatabase
  {
    public const string Empty = "";

    public int ActorSystemTcpPortNumber { get; private set; } = 0;

    public ManagerFormTreeView MnFormTreeView { get; internal set; } = null;

    public ManagerOfActorSystem MnActorSystem { get; internal set; } = ManagerOfActorSystem.Create();

    public FormTreeView TvForm { get; private set; } = null;

    public ServiceFormTreeView TvService { get; private set; } = null;

    public RadTreeView TvFolders { get; internal set; } = null;

    public IActorRef AcMain { get; internal set; } = null;

    public void OutputMessage(string message, string header = Empty)
    {
      Ms.Message(message, header).NoAlert().Debug();
    }

    internal int GetIdFolder(RadTreeNode node) => CxConvert.ToInt32(node.Value, short.MinValue);


    internal void EventPageChanged(string name)
    {

    }

    internal void EventAfterAllFormsAreCreated()
    {
      StartActorSystem();
    }

    internal void EventStartWork()
    {
      InitMembers();
    }

    internal void InitMembers()
    {
      if (MnFormTreeView.TvFormMaintainer == null) MessageBox.Show("NULL!!! MnFormTreeView.TvFormMaintainer");
      TvForm = MnFormTreeView.TvFormMaintainer.Form;
      TvService = MnFormTreeView.TvFormMaintainer;
      TvFolders = MnFormTreeView.TvFormMaintainer.Form.TvFolders;
      AcMain = MnActorSystem.AxCommander;
    }


    internal void StartActorSystem()
    {
      MnActorSystem.Configure(this);
      Exception ex = MnActorSystem.StartActorSystem
      (
      AppSettings.SettingsDatabaseLocation, // TODO: возможно следует использовать хэш от имени файла БД для имени коммандера или имени акторной системы //
      AppSettings.IpAddressOfActorSystem,
      AppSettings.TcpPortOfActorSystem,
      "commander" // TODO: commander name should be secret // Ulid.NewUlid().ToString().Right(19)
      );

      if (ex != null)
      {
        Ms.Error("Error! Could not start actor system", ex).NoAlert().Error();
        return;
      }

      Ms.Message("Actor system started", MnActorSystem.GetActorSystemConfig()).NoAlert().Debug();
    }

    internal void EventBeforeMainFormClose()
    {

    }


    public RadTreeNode CreateNode(Folder folder)
    {
      RadTreeNode node = new RadTreeNode(folder.NameFolder);
      node.Value = folder.IdFolder;
      return node;
    }

    public void CheckNodeChildren(RadTreeNode node, List<Folder> folders)
    {
      foreach (var folder in folders)
      {
        if (node.Nodes.Contains(folder.NameFolder))
        {
          RadTreeNode child = node.Nodes[node.Nodes.IndexOf(folder.NameFolder)];
          if (GetIdFolder(child) != folder.IdFolder) child.Value = folder.IdFolder;
        }
        else
        {
          RadTreeNode child = CreateNode(folder);
          node.Nodes.Add(child);
        }
      }

      for (int i = node.Nodes.Count - 1; i >= 0; --i)
      {
        RadTreeNode child = node.Nodes[i];
        int IdFolder = GetIdFolder(child);
        if ((folders.Exists(f => f.IdFolder == IdFolder)) == false)
        {
          node.Nodes.RemoveAt(i);
        }
      }
    }

    public void ReportActorSystemTcpPort(int PortNumber)
    {
      Ms.Message($"Actor system tcp port = {PortNumber}", "").NoAlert().Debug();
      ActorSystemTcpPortNumber = PortNumber;
    }

    public void ReportDatabaseConnection(AskOpenDatabase request, ReturnCode code)
    {
      if (code.Success)
        Ms.Message($"Connection to local database is established", request.FileName).NoAlert().Debug();
      else
        Ms.Message($"Connection to local database is not established", code.Message).NoAlert().Error();
    }

    public void ReportTvRootNode(AnsTvRootNode arg)
    {
      if (!((TvFolders.Nodes.Count == 0) && (arg.Folder.IdFolder == 0))) return;

      RadTreeNode child;
      List<RadTreeNode> children = new List<RadTreeNode>();

      foreach (var item in arg.Folders) // TODO: Нужно настроить шрифт и картинки - затем это выражение нужно упростить //
      {
        child = CreateNode(item);
        children.Add(child);
      }

      RadTreeNode RootNode = new RadTreeNode(arg.Folder.NameFolder, children.ToArray());
      RootNode.Value = arg.Folder.IdFolder;
      TvFolders.Nodes.Add(RootNode);
      RootNode.Expand();

    }

    public void ReportFolderInfo(AnsFolderInfo arg)
    {
      RadTreeNode node = TvFolders.SelectedNode;
      if (arg.Folder.IdFolder == GetIdFolder(node))
      {
        CheckNodeChildren(node, arg.Folders);
      }
    }

    public void RequestLoadData()
    {
      TvFolders.Nodes.Clear();
      /*
      TvFolders.Nodes.Add("xa xa xa");
      TvFolders.Nodes[0].Nodes.Add("TEst 1");
      TvFolders.Nodes[0].Nodes.Add("TEst 2");
      */
      AcMain.Tell(AskTvRootNode.Create());
    }

    public void RequestTreeviewSelectedNodeChanged(object sender, RadTreeViewEventArgs e)
    {
      int IdFolder = GetIdFolder(e.Node);
      AcMain.Tell(AskFolderInfo.Create(IdFolder));
    }
  }
}