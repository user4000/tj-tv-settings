﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Windows.Forms;
using Akka.Actor;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using TJFramework;
using TJStandard;
using TJSettingsManagement;
using static TJExampleSettings.Program;
using static TJFramework.TJFrameworkManager;

namespace TJExampleSettings
{
  public partial class FxTest1 : RadForm, IEventStartWork, IOutputMessage
  {
    public FxTest1()
    {
      InitializeComponent();
    }

    public void EventStartWork()
    {
      BxTest1.Click += EventAutoDetectTcpPortOfAnActorSystem;
    }

    public void OutputMessage(string message, string header = "") => Print(message, header);

    public void Print(string message, string header = "")
    {
      if (TxMessage.InvokeRequired)
        TxMessage.Invoke((MethodInvoker)delegate { PrintInner(message, header); });
      else
        PrintInner(message, header);
    }

    private void PrintInner(string message, string header = "")
    {
      if (message=="clear") { TxMessage.Clear(); return; }
      TxMessage.AppendText((header + " " + message).Trim() + Environment.NewLine);
    }

    private void EventAutoDetectTcpPortOfAnActorSystem(object sender, EventArgs e)
    {
      /*
      Manager.MnActorSystem.AxCommander.Tell(this);
      Manager.MnActorSystem.AxCommander.Tell("Hello my friend");
      AskDetectActorSystemRemotePort request = AskDetectActorSystemRemotePort.Create
        (
        Manager.MnActorSystem.AcSystem, 
        Manager.MnActorSystem.ActorSystemIpAddress, 
        Manager.MnActorSystem.NameOfActorCommander
        );
      Manager.MnActorSystem.AxCommander.Tell(request); 
      */   

    }
  }
}
