﻿using System;
using System.Data;
using System.Data.SQLite;
using TJStandard;

namespace TJSettings
{
  public partial class LocalDatabaseOfSettings
  {
    private void SetPathToDatabase(string PathToDatabase, string ConnectionConfig = "")
    {
      ConnectionString = $"Data Source={PathToDatabase};" +
        (String.IsNullOrWhiteSpace(ConnectionConfig) ? ConnectionConfigDefault : ConnectionConfig);
    }
 
    public SQLiteConnection GetSqliteConnection() => new SQLiteConnection(ConnectionString);

    public DataTable GetTable(string TableName)
    {
      DataTable table = new DataTable();
      using (SQLiteConnection connection = GetSqliteConnection())
      using (SQLiteCommand command = connection.ZzCommand($"SELECT * FROM {TableName}"))
      using (SQLiteDataReader reader = command.ExecuteReader())
      {
        table.Load(reader);
      }
      return table;
    }

    public DataTable GetTableFolders()
    {
      DataTable table = GetTable(DbEngine.TnFolders);
      foreach (DataColumn column in table.Columns) if (column.ColumnName == DbEngine.CnFoldersIdParent) column.AllowDBNull = true;
      return table;
    }

    private string GetScalarString(string SqlScalarStringQuery)
    {
      using (SQLiteConnection connection = GetSqliteConnection())
      using (SQLiteCommand command = connection.ZzCommand(SqlScalarStringQuery))
      {
        return command.ZzGetScalarString();
      }
    }

    public ReturnCode ConnectToDatabase(string PathToDatabase, string ConnectionConfig)
    {
      if (PathToDatabase == string.Empty)
      {
        return ReturnCodeFactory.Error("Database file path not specified");
      }

      SetPathToDatabase(PathToDatabase, ConnectionConfig);
      ReturnCode code;
      try
      {
        code = CheckDatabaseStructure();
      }
      catch
      {
        return ReturnCodeFactory.Error("The file you specified is not a SQLite database.");
      }

      if (code.Error) return code;
    
      try
      {
        RootFolderName = GetRootFolderName();
      }
      catch (Exception ex)
      {
        RootFolderName = string.Empty;
        return ReturnCodeFactory.Error("Could not read data from the database file.", ex.Message);
      }
      return ReturnCodeFactory.Success();
    }

    private ReturnCode CheckDatabaseStructure()
    {
      ReturnCode code = ReturnCodeFactory.Success($"Database structure is ok");
      int CheckObjectCount = 4;
      using (SQLiteConnection connection = GetSqliteConnection())
      using (SQLiteCommand command = connection.ZzCommand(DbEngine.SqlCheckDatabaseStructure))
      {        
        if (command.ZzGetScalarInteger() != CheckObjectCount)
          code = ReturnCodeFactory.Error
            (
            ReturnCodeFactory.NcError, 
            "The database structure is not compliant with the standard. When working with the database errors may occur."
            );
      }
      return code;
    }

    public ReturnCode CreateNewDatabase(string text) => DbEngine.CreateNewDatabase(text);

  }
}

