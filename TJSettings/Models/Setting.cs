﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*
This project requires these packages:

Retrieving package 'System.Data.SQLite.Core 1.0.112.1' from 'nuget.org'.
  GET https://api.nuget.org/v3-flatcontainer/system.data.sqlite.core/1.0.112.1/system.data.sqlite.core.1.0.112.1.nupkg
  OK https://api.nuget.org/v3-flatcontainer/system.data.sqlite.core/1.0.112.1/system.data.sqlite.core.1.0.112.1.nupkg 119ms
Installing System.Data.SQLite.Core 1.0.112.1.

*/


namespace TJSettings
{
  [Serializable]
  public class Setting
  {
    public int IdFolder { get; set; }

    public string IdSetting { get; set; }

    public int IdType { get; set; }

    public string NameType { get; set; }

    public string SettingValue { get; set; }

    public int Rank { get; set; }

    public string BooleanValue { get; set; }

    public Setting(int idFolder, string idSetting, int idType, string nameType, string settingValue, int rank, string booleanValue)
    {
      IdFolder = idFolder;
      IdSetting = idSetting;
      IdType = idType;
      NameType = nameType;
      SettingValue = settingValue;
      Rank = rank;
      BooleanValue = booleanValue;
    }

    public static Setting Create(int idFolder, string idSetting, int idType, string settingValue, int rank)
    {
      return new Setting(idFolder, idSetting, idType, string.Empty, settingValue, rank, string.Empty);
    }
  }
}

