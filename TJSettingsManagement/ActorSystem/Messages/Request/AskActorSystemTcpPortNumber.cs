﻿using Akka.Actor;

namespace TJSettingsManagement
{
  public class AskActorSystemTcpPortNumber
  {
    public ActorSystem AcSystem { get; } = null;

    public string IpAddress  { get; } = string.Empty;

    public string NameOfActorSystem { get; } = string.Empty;

    public string NameOfCommander { get; } = string.Empty;

    public AskActorSystemTcpPortNumber(ActorSystem actorSystem, string ipAddress, string nameOfCommander)
    {
      AcSystem = actorSystem; IpAddress = ipAddress; NameOfActorSystem = AcSystem.Name; NameOfCommander = nameOfCommander;    
    }

    public static AskActorSystemTcpPortNumber Create(ActorSystem actorSystem, string ipAddress, string nameOfCommander) 
      => new AskActorSystemTcpPortNumber(actorSystem, ipAddress, nameOfCommander);

  }
}