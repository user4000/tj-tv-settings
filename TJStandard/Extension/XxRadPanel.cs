﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;

namespace TJStandard
{
  public static class XxRadPanel
  {
    public static void ZzAddTestButton(this RadPanel panel, string ButtonText, Point Location, Size Size, Action ButtonClickHandler)
    {
      RadButton btn = new RadButton();
      btn.Text = ButtonText;   
      panel.Controls.Add(btn);
      btn.Location = Location;
      btn.Size = Size;
      btn.BringToFront();
      btn.Click += (s, e) => ButtonClickHandler();
    }

    public static void ZzHideBorder(this RadPanel panel)
    {
      panel.PanelElement.PanelBorder.Visibility = ElementVisibility.Collapsed;
    }
  }
}