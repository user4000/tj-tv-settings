﻿using System;
using System.ComponentModel;
using System.Drawing;
using Telerik.WinControls.UI;
using TJFramework;
using TJFramework.ApplicationSettings;
using TJSettingsFormTreeView;
using static TJFramework.Logger.Manager;

namespace TJSettingsFormTreeView
{
  [Serializable]
  public class FormTreeViewApplicationSettings : TJStandardApplicationSettings
  {
    [Category("Settings Database")]
    [DisplayName("File location")]
    [Editor(typeof(PropertyGridBrowseEditor), typeof(BaseInputEditor))] // File name dialog //
    public string SettingsDatabaseLocation { get; set; }

    [Category("Settings Database")]
    [DisplayName("Folder search mode")]
    public TextSearchMode FolderNameSearchMode { get; set; } = TextSearchMode.StartWith;

    [Category("Settings Database")]
    [DisplayName("Select a new folder after creation")]
    public bool SelectNewFolderAfterCreating { get; set; } = false;

    [Category("Settings Database")]
    [DisplayName("Default new database file name")]
    public string NewFileName { get; set; } = "settings.db";

    [Category("User interface")]
    [DisplayName("Font of a hierarchical folder list")]
    public Font TreeViewFont { get; set; } = new Font("Verdana", 9.75F);

    [Category("User interface")]
    [DisplayName("Auto hide [Advanced settings] tab")]
    public virtual bool AutoHideTreeViewSettingsTab { get; set; } = false;

    [Browsable(false)]
    public Size TreeViewSize { get; set; } = new Size(400, 0);

    [Category("Setting value editor")]
    [DisplayName("Allow direct editing of \"File name\" text field")]
    public bool AllowEditSettingFileName { get; set; } = true;

    [Category("Setting value editor")]
    [DisplayName("Allow direct editing of \"Folder name\" text field")]
    public bool AllowEditSettingFolderName { get; set; } = true;

    [Category("Network")]
    [DisplayName("TCP server port number")]
    public int TcpPortOfActorSystem { get; set; } = 0;

    [Category("Network")]
    [DisplayName("TCP server IP address")]
    public string IpAddressOfActorSystem { get; set; } = "127.0.0.1";

    public override void PropertyValueChanged(string PropertyName)
    {

    }

    public override void EventBeforeSaving()
    {

    }

    public override void EventAfterSaving()
    {

    }
  }
}
