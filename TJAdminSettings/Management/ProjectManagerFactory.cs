﻿using System.Windows.Forms;
using TJStandard;
using static TJAdminSettings.Program;

namespace TJAdminSettings

{
  public class ProjectManagerFactory
  {
    public static ProjectManager Create()
    {
      ProjectManager manager = new ProjectManager();
      return manager;
    }
  }
}