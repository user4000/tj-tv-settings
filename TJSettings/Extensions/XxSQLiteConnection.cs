﻿using System.Data.SQLite;

namespace TJSettings
{
  public static class XxSQLiteConnection
  {
    public static SQLiteCommand ZzCommand(this SQLiteConnection connection)
    {
      connection.Open(); return new SQLiteCommand(connection);
    }

    public static SQLiteCommand ZzCommand(this SQLiteConnection connection, string CommandText)
    {
      connection.Open(); return new SQLiteCommand(CommandText, connection);
    }
  }
}

