﻿using System;
using System.Net;
using System.Net.NetworkInformation;
using Akka.Actor;

namespace TJSettingsManagement
{
  public class AcTcpPortDetector : ReceiveActor
  {
    int PortNumber { get; set; } = 0;

    public AcTcpPortDetector()
    {
      Receive<AskCheckTcpPortNumber>(arg => EventAskSearchingTcpPortNumber(arg));
      Receive<AskActorSystemTcpPortNumber>(arg => EventAskDetectActorSystemRemotePortAsync(arg));
    }

    void EventAskSearchingTcpPortNumber(AskCheckTcpPortNumber arg)
    {    
      if ((arg.PortNumber > 0) && (PortNumber == 0))
      {
        PortNumber = arg.PortNumber;       
      }
      Sender.Tell(AnsActorSystemTcpPortNumber.Create(PortNumber, arg.RemotePath));
    }

    void EventAskDetectActorSystemRemotePortAsync(AskActorSystemTcpPortNumber arg)
    {
      string path;
      string path1 = $"akka.tcp://{arg.NameOfActorSystem}@{arg.IpAddress}:";
      string path2 = $"/user/{arg.NameOfCommander}";
      // Detecting tcp port that is being listened by instance of an [ActorSystem] class //
      IPGlobalProperties properties = IPGlobalProperties.GetIPGlobalProperties();
      IPEndPoint[] endPoints = properties.GetActiveTcpListeners();
      ActorSelection RemoteActors;
      foreach (IPEndPoint item in endPoints)
      {
        if (PortNumber > 0) break;
        if (item.Address.ToString() == arg.IpAddress)              
          try
          {
            path = path1 + item.Port.ToString() + path2;
            RemoteActors = arg.AcSystem.ActorSelection(path);
            RemoteActors.Tell(AskCheckTcpPortNumber.Create(item.Port, path));
          }
          catch
          {

          }
        
      }
    }
  }
}