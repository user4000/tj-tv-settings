﻿using Telerik.WinControls;
using Telerik.WinControls.UI;

namespace TJStandard
{
  public static class XxRadPageViewPage
  {
    public static void ZzShow(this RadPageViewPage page, bool Show)
    {
      if (Show)
      {
        if (page.Item.Visibility != ElementVisibility.Visible) page.Item.Visibility = ElementVisibility.Visible;
      }
      else
      {
        if (page.Item.Visibility != ElementVisibility.Collapsed) page.Item.Visibility = ElementVisibility.Collapsed;
      }
    }
  }
}