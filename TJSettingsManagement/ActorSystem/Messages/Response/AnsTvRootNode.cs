﻿using System.Collections.Generic;
using TJSettings;

namespace TJSettingsManagement
{
  public class AnsTvRootNode
  {

    public Folder Folder { get; } = null;

    public List<Folder> Folders { get; } = null;

    public AnsTvRootNode(Folder folder, List<Folder> folders)
    {
      Folder = folder;
      Folders = folders;
    }

    public static AnsTvRootNode Create(Folder folder, List<Folder> folders) => new AnsTvRootNode(folder, folders);
  }
}