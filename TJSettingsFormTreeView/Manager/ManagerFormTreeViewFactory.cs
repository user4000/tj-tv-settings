﻿using System.Windows.Forms;
using TJFramework;
using TJSettings;
using TJStandard;

namespace TJSettingsFormTreeView
{
  public class ManagerFormTreeViewFactory
  {
    public static ManagerFormTreeView Create(FormTreeViewApplicationSettings settings, FormTreeViewMode mode, ISendRequestToDatabase requestToDatabaseSender)
    {
      ManagerFormTreeView manager = new ManagerFormTreeView();
      manager.Settings = settings; // Very important. This is a client application settings //
      manager.OneOrManyDatabases = mode;
      manager.RequestToDb = requestToDatabaseSender;

      TJFrameworkManager.Service.EventPageChanged += manager.EventPageChanged;
      TJFrameworkManager.Service.EventAfterAllFormsAreCreated += manager.EventAfterAllFormsAreCreated;
      TJFrameworkManager.Service.EventBeforeMainFormClose += manager.EventBeforeMainFormClose;

      return manager;
    }
  }
}