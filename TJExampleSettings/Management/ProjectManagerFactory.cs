﻿using System.Windows.Forms;
using TJStandard;
using static TJExampleSettings.Program;

namespace TJExampleSettings
{
  public class ProjectManagerFactory
  {
    public static ProjectManager Create()
    {
      ProjectManager manager = new ProjectManager();
      return manager;
    }
  }
}