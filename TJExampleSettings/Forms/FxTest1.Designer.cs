﻿namespace TJExampleSettings
{
    partial class FxTest1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
      this.PvTest = new Telerik.WinControls.UI.RadPageView();
      this.radPageViewPage1 = new Telerik.WinControls.UI.RadPageViewPage();
      this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
      this.radPanel2 = new Telerik.WinControls.UI.RadPanel();
      this.TxMessage = new Telerik.WinControls.UI.RadTextBox();
      this.radTextBoxControl1 = new Telerik.WinControls.UI.RadTextBoxControl();
      this.BxTest1 = new Telerik.WinControls.UI.RadButton();
      ((System.ComponentModel.ISupportInitialize)(this.PvTest)).BeginInit();
      this.PvTest.SuspendLayout();
      this.radPageViewPage1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
      this.radPanel1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).BeginInit();
      this.radPanel2.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.TxMessage)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.radTextBoxControl1)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.BxTest1)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
      this.SuspendLayout();
      // 
      // PvTest
      // 
      this.PvTest.Controls.Add(this.radPageViewPage1);
      this.PvTest.Dock = System.Windows.Forms.DockStyle.Fill;
      this.PvTest.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.PvTest.Location = new System.Drawing.Point(0, 0);
      this.PvTest.Name = "PvTest";
      this.PvTest.SelectedPage = this.radPageViewPage1;
      this.PvTest.Size = new System.Drawing.Size(1132, 766);
      this.PvTest.TabIndex = 0;
      ((Telerik.WinControls.UI.RadPageViewStripElement)(this.PvTest.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.Scroll;
      // 
      // radPageViewPage1
      // 
      this.radPageViewPage1.Controls.Add(this.radPanel2);
      this.radPageViewPage1.Controls.Add(this.radPanel1);
      this.radPageViewPage1.ItemSize = new System.Drawing.SizeF(55F, 28F);
      this.radPageViewPage1.Location = new System.Drawing.Point(10, 37);
      this.radPageViewPage1.Name = "radPageViewPage1";
      this.radPageViewPage1.Size = new System.Drawing.Size(1111, 718);
      this.radPageViewPage1.Text = "Test 1";
      // 
      // radPanel1
      // 
      this.radPanel1.Controls.Add(this.BxTest1);
      this.radPanel1.Controls.Add(this.radTextBoxControl1);
      this.radPanel1.Dock = System.Windows.Forms.DockStyle.Top;
      this.radPanel1.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.radPanel1.Location = new System.Drawing.Point(0, 0);
      this.radPanel1.Name = "radPanel1";
      this.radPanel1.Size = new System.Drawing.Size(1111, 194);
      this.radPanel1.TabIndex = 0;
      // 
      // radPanel2
      // 
      this.radPanel2.Controls.Add(this.TxMessage);
      this.radPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
      this.radPanel2.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.radPanel2.Location = new System.Drawing.Point(0, 194);
      this.radPanel2.Name = "radPanel2";
      this.radPanel2.Padding = new System.Windows.Forms.Padding(5);
      this.radPanel2.Size = new System.Drawing.Size(1111, 524);
      this.radPanel2.TabIndex = 0;
      // 
      // TxMessage
      // 
      this.TxMessage.Dock = System.Windows.Forms.DockStyle.Fill;
      this.TxMessage.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.TxMessage.Location = new System.Drawing.Point(5, 5);
      this.TxMessage.Multiline = true;
      this.TxMessage.Name = "TxMessage";
      // 
      // 
      // 
      this.TxMessage.RootElement.StretchVertically = true;
      this.TxMessage.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
      this.TxMessage.Size = new System.Drawing.Size(1101, 514);
      this.TxMessage.TabIndex = 0;
      // 
      // radTextBoxControl1
      // 
      this.radTextBoxControl1.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.radTextBoxControl1.Location = new System.Drawing.Point(19, 16);
      this.radTextBoxControl1.Name = "radTextBoxControl1";
      this.radTextBoxControl1.Size = new System.Drawing.Size(633, 29);
      this.radTextBoxControl1.TabIndex = 0;
      // 
      // BxTest1
      // 
      this.BxTest1.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.BxTest1.Location = new System.Drawing.Point(689, 16);
      this.BxTest1.Name = "BxTest1";
      this.BxTest1.Size = new System.Drawing.Size(130, 28);
      this.BxTest1.TabIndex = 1;
      this.BxTest1.Text = "Test 1";
      // 
      // FxTest1
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(1132, 766);
      this.Controls.Add(this.PvTest);
      this.Name = "FxTest1";
      // 
      // 
      // 
      this.RootElement.ApplyShapeToControl = true;
      this.Text = "FxTest1";
      ((System.ComponentModel.ISupportInitialize)(this.PvTest)).EndInit();
      this.PvTest.ResumeLayout(false);
      this.radPageViewPage1.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
      this.radPanel1.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).EndInit();
      this.radPanel2.ResumeLayout(false);
      this.radPanel2.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.TxMessage)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.radTextBoxControl1)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.BxTest1)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
      this.ResumeLayout(false);

        }

    #endregion

    private Telerik.WinControls.UI.RadPageView PvTest;
    private Telerik.WinControls.UI.RadPageViewPage radPageViewPage1;
    public Telerik.WinControls.UI.RadPanel radPanel2;
    public Telerik.WinControls.UI.RadTextBox TxMessage;
    private Telerik.WinControls.UI.RadPanel radPanel1;
    private Telerik.WinControls.UI.RadButton BxTest1;
    private Telerik.WinControls.UI.RadTextBoxControl radTextBoxControl1;
  }
}
