﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Akka.Actor;
using TJSettings;

namespace TJSettingsManagement
{
  public class AcFolder : ReceiveActor
  {
    IActorRef DbOperator { get; set; } = null;

    Folder Folder { get; set; } = null;

    List<Folder> Folders { get; set; } = null;

    public AcFolder()
    {
      Receive<AskFolderActorConfigure>(arg => ReqFolderActorConfigure(arg));
      Receive<AnsFolderActorConfigure>(arg => ResFolderActorConfigure(arg));
      Receive<AskTvRootNode>(arg => ReqAskTvRootNode(arg));
      Receive<AskFolderInfo>(arg => ReqAskFolderInfo(arg));
      //Trace.WriteLine($"Actor Folder [CONSTRUCTOR]: {Self.Path}");
    }

    void ReqFolderActorConfigure(AskFolderActorConfigure arg)
    {     
      Folder = arg.Folder; 
      DbOperator = arg.DbOperator;
      DbOperator.Tell(arg);
      //Trace.WriteLine($"[ReqFolderActorConfigure]: {Self.Path} --- {DbOperator.Path} --- {Folder.ToString()}");
    }

    void ResFolderActorConfigure(AnsFolderActorConfigure arg)
    {
      IActorRef folder;
      Folders = arg.Folders;
      foreach (var item in Folders)
      {
        folder = Context.ActorOf<AcFolder>(item.IdFolder.ToString());
        folder.Tell(AskFolderActorConfigure.Create(DbOperator, item));
      }
      // TODO: Add Settings;
    }

    void ReqAskTvRootNode(AskTvRootNode arg)
    {
      if (Folder.IdFolder == 0)
      {
        DbOperator.Tell(AnsTvRootNode.Create(Folder, Folders));
      }
    }

    void ReqAskFolderInfo(AskFolderInfo arg)
    {
      if (arg.IdFolder == Folder.IdFolder) // Yes it's me //
       DbOperator.Tell(AnsFolderInfo.Create(Folder, Folders));
      else     
       ReqAnsFolderInfoToAllChildren(arg);    
    }

    void ReqAnsFolderInfoToAllChildren(AskFolderInfo arg)
    {
      IEnumerable<IActorRef> list = Context.GetChildren();
      foreach (var actor in list) actor.Tell(arg);      
    }
  }
}