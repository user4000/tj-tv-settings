﻿using System.Windows.Forms;
using Telerik.WinControls.UI;
using TJSettings;
using TJStandard;
using static TJFramework.TJFrameworkManager;

namespace TJSettingsFormTreeView
{
  public class ManagerFormTreeView
  {
    public const string Empty = "";

    public Converter CvManager { get; } = new Converter();

    public FormTreeViewApplicationSettings Settings { get; internal set; } = null;

    public ServiceFormTreeView TvFormMaintainer { get; private set; } = null;

    public FormTreeView RfFormTreeView { get; private set; } = null;

    public RadPageViewPage PgTreeView { get; private set; } = null;

    public FormTreeViewMode OneOrManyDatabases { get; internal set; }

    public ISendRequestToDatabase RequestToDb { get; internal set; } = null;

    public void EventPageChanged(string name)
    {
      if (name.Contains(nameof(TJFramework.Form.FxExit))) return;
      /* 
        if (TvFormMaintainer.WorkWithOnlyOneDatabase)
        PgTreeView.ZzShow // "Advanced settings" page is visible only if user visits "Settings" page //
          (
          name.Contains(nameof(FormTreeView)) ||
          name.Contains(nameof(TJFramework.Form.FxSettings)) ||
          (!Settings.AutoHideTreeViewSettingsTab)
          );
      */
      TvFormMaintainer.EventPageChanged(name);
    }

    public void EventAfterAllFormsAreCreated()
    {
      RfFormTreeView = Pages.GetRadForm<FormTreeView>();
      TvFormMaintainer = ServiceFormTreeView.Create(this, OneOrManyDatabases);
      PgTreeView = Pages.GetPageByUniqueName(Pages.GetUniquePageName<FormTreeView>());
      //PgTreeView.ZzShow(!Settings.AutoHideTreeViewSettingsTab);
    }

    public void EventBeforeMainFormClose()
    {      
      TvFormMaintainer.EventEndWork();
    }
  }
}