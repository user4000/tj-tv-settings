﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Telerik.WinControls.UI.Docking;
using TJFramework;
using TJSettings;
using TJStandard;
using static TJFramework.TJFrameworkManager;

namespace TJSettingsFormTreeView
{
  public class ServiceFormTreeView 
  {
    private int HeightCollapsed { get; } = 82;
    private int HeightExpanded { get; } = 126;
    private int HeightForLongText { get; } = 200; // default = 200;
    private ServiceTreeviewControl TvManager { get; set; } = null;
    private ServiceGridControl VxGridSettings { get; set; } = null;
    private string NameOfSelectedNode { get; set; } = string.Empty;
    private RadTreeNode[] SearchResult { get; set; } = null;
    private Setting CurrentSetting { get; set; } = null;
    private string CurrentIdSetting { get; set; } = string.Empty;

    private ISendRequestToDatabase RequestToDb { get; set; } = null;

    private static readonly int idFolderNoSelection = -1;

    private int CurrentIdFolder { get; set; } = idFolderNoSelection;

    private string CurrentFolderPath { get; set; } = string.Empty;

    private int IdFolderNoValue { get; } = idFolderNoSelection;

    private int SearchIterator { get; set; } = 0;

    public bool AdminMode { get; private set; } = false;

    public bool FlagAllowChangeSelectedFolder { get; private set; } = true;

    public void AllowChangeSelectedFolder(bool allow) => FlagAllowChangeSelectedFolder = allow;

    public FormTreeView Form { get; private set; } = null;

    public ManagerFormTreeView Manager { get; private set; } = null;

    private ServiceFormTreeView(ManagerFormTreeView manager)
    {
      Manager = manager;
      Form = manager.RfFormTreeView;
      RequestToDb = manager.RequestToDb;
    }

    public static ServiceFormTreeView Create(ManagerFormTreeView manager, FormTreeViewMode mode)
    {
      ServiceFormTreeView TvFormMaintainer = new ServiceFormTreeView(manager);
      TvFormMaintainer.Init();
      TvFormMaintainer.AdminMode = mode == FormTreeViewMode.SettingsDatabaseAdministrator;
      TvFormMaintainer.TvManager = ServiceTreeviewControl.Create(manager);

      if (!TvFormMaintainer.AdminMode)
      {
        TvFormMaintainer.EventAfterAllFormsAreCreated();
      }

      TvFormMaintainer.UserCanChangeDatabase(TvFormMaintainer.AdminMode);
      return TvFormMaintainer;
    }

    internal void EventAfterAllFormsAreCreated()
    {
      // This method places page with "TreeView" form AFTER already created "Settings" page //
      var pageView = TJFrameworkManager.Service.MainPageView;
      var pages = pageView.Pages;
      RadPageViewPage PgSettings = null;
      RadPageViewPage PgTreeView = null;
      RadPageViewPage PgLog = null;
      foreach (var page in pages)
      {
        if (page.Name.Contains(nameof(TJFramework.Form.FxSettings))) PgSettings = page;
        if (page.Name.Contains(nameof(TJFramework.Form.FxLog))) PgLog = page;
        if (page.Name.Contains(nameof(FormTreeView))) PgTreeView = page;
      }
      int index = pageView.Pages.IndexOf(PgLog);
      pageView.Pages.ChangeIndex(PgLog, index - 1);

      index = pageView.Pages.IndexOf(PgSettings);
      pageView.Pages.ChangeIndex(PgTreeView, index);
    }

    private void Init()
    {
      SetProperties();
      SetEvents();
    }

    private void SetProperties()
    {
      Padding NoPadding = new Padding(0, 0, 0, 0);
      Form.BxOpenFile.ShowBorder = false;
      Form.BxSelectFile.ShowBorder = false;
      Form.BxFolderAdd.ShowBorder = false;
      Form.BxFolderRename.ShowBorder = false;
      Form.BxFolderDelete.ShowBorder = false;
      Form.BxFolderSearch.ShowBorder = false;
      Form.BxCopyFolderPath.ShowBorder = false;
      Form.BxGridRefresh.ShowBorder = false;
      Form.BxFolderSearchGotoNext.ShowBorder = false;
      Form.BxFolderSearchGotoNext.Visibility = ElementVisibility.Collapsed;
      Form.BxSettingAddNew.ShowBorder = false;
      Form.BxSettingRename.ShowBorder = false;
      Form.BxSettingFileSelect.ShowBorder = false;
      Form.BxSettingColorSelect.ShowBorder = false;
      Form.BxSettingFontSelect.ShowBorder = false;
      Form.BxSettingFolderSelect.ShowBorder = false;
      Form.BxSettingDelete.ShowBorder = false;
      Form.BxSettingChange.Enabled = false;
      Form.BxSettingSave.Visible = false;
      Form.BxSettingCancel.Visible = false;
      Form.BxNewFileName.ShowBorder = false;
      Form.BxCreateNewDatabase.ShowBorder = false;

      Form.BxCopyFolderPath.ToolTipText = "Copy to clipboard";
      Form.BxCopyFolderPath.AutoToolTip = true;

      Form.BxGridRefresh.ToolTipText = "Refresh list of settings";
      Form.BxGridRefresh.AutoToolTip = true;

      SetPropertiesDateTimePicker();

      Form.TxDatabaseFile.ReadOnly = true;
      Form.TxFolderDelete.ReadOnly = true;

      Form.StxFolder.ReadOnly = !Manager.Settings.AllowEditSettingFolderName;
      Form.StxFile.ReadOnly = !Manager.Settings.AllowEditSettingFileName;

      Form.PvEditor.SelectedPage = Form.PgEmpty;
      Form.PvEditor.ZzPagesVisibility(ElementVisibility.Collapsed);

      Form.PvFolders.Pages.ChangeIndex(Form.PgFolderDelete, 4);
      Form.PvFolders.Pages.ChangeIndex(Form.PgFolderRename, 3);
      Form.PvFolders.Pages.ChangeIndex(Form.PgFolderAdd, 2);
      Form.PvFolders.Pages.ChangeIndex(Form.PgFolderSearch, 1);
      Form.PvFolders.Pages.ChangeIndex(Form.PgDatabase, 0);
      Form.PvFolders.SelectedPage = Form.PgDatabase;

      Form.BxSettingUp.Left = Form.BxSettingCancel.Location.X + Form.BxSettingCancel.Size.Width + 2 * Form.BxSettingUp.Size.Width;
      Form.BxSettingDown.Left = Form.BxSettingCancel.Location.X + Form.BxSettingCancel.Size.Width + 4 * Form.BxSettingUp.Size.Width;

      Form.PvSettings.Pages.ChangeIndex(Form.PgSettingChange, 0);
      Form.PvSettings.SelectedPage = Form.PgSettingChange;

      Form.PnTreeview.SizeInfo.SizeMode = SplitPanelSizeMode.Absolute;
      Form.PnTreeview.SizeInfo.AbsoluteSize = Manager.Settings.TreeViewSize;

      Form.PnSettingAddTool.PanelElement.PanelBorder.Visibility = ElementVisibility.Hidden;
      Form.PnSettingChangeTool.PanelElement.PanelBorder.Visibility = ElementVisibility.Hidden;
      Form.PnSettingAddTop.PanelElement.PanelBorder.Visibility = ElementVisibility.Hidden;
      Form.PnSettingChangeTop.PanelElement.PanelBorder.Visibility = ElementVisibility.Hidden;

      Form.StxLongInteger.ZzSetIntegerNumberOnly();
      Form.StxDatetime.CalendarSize = new Size(400, 350);

      VxGridSettings = new ServiceGridControl(this.Manager);
      VxGridSettings.InitializeGrid(Form.GvSettings);

      Form.PgSettingEmpty.Item.Visibility = ElementVisibility.Collapsed;
      Form.PgSettingMessage.Item.Visibility = ElementVisibility.Collapsed;

      Form.PvEditor.Padding = new Padding(0, 1, 0, 0);
      Form.PvEditor.Margin = NoPadding;

      Form.PnSettingAddTool.Padding = NoPadding;
      Form.PnSettingAddTool.Margin = NoPadding;

      Form.PnSettingChangeTool.Padding = NoPadding;
      Form.PnSettingChangeTool.Margin = NoPadding;

      Form.StxDatetime.Value = DateTime.Today;
      Form.TvFolders.ShowRootLines = false;

      SetDatabaseFile(Manager.Settings.SettingsDatabaseLocation);
    }

    private void SetEvents()
    {
      Form.BxOpenFile.Click += EventLoadData; // Open a database file //
      Form.BxSelectFile.Click += EventButtonChooseFile;
      Form.BxFolderAdd.Click += EventButtonAddFolder;
      Form.BxFolderRename.Click += EventButtonRenameFolder;
      Form.BxFolderDelete.Click += EventButtonDeleteFolder;
      Form.BxFolderSearch.Click += EventButtonSearchFolder;
      Form.BxFolderSearchGotoNext.Click += EventButtonSearchFolderGotoNext;

      Form.BxSettingAddNew.Click += EventButtonSettingAddNew; // SAVE setting (INSERT) //
      Form.BxSettingSave.Click += EventButtonSettingUpdateExisting; // SAVE setting (UPDATE) //
      Form.BxSettingDelete.Click += EventButtonSettingDelete; // Delete setting //
      Form.BxSettingRename.Click += EventButtonSettingRename; // Rename setting //
      Form.BxSettingChange.Click += EventButtonSettingChange; // Change value of setting // it is async method and await works inside it //
      Form.BxSettingCancel.Click += EventButtonSettingCancel; // Cancel changing setting //
      Form.BxSettingUp.Click += EventButtonSettingUp; // Change Rank - go up //
      Form.BxSettingDown.Click += EventButtonSettingDown; // Change Rank - go down //

      Form.BxNewFileName.Click += EventButtonNewFileName;
      Form.BxCreateNewDatabase.Click += EventButtonCreateNewDatabase;

      Form.PvSettings.SelectedPageChanging += EventForAllPageViewSelectedPageChanging;
      Form.PvFolders.SelectedPageChanging += EventForAllPageViewSelectedPageChanging;
      Form.PvSettings.SelectedPageChanging += EventForAllPageViewSelectedPageChanging;

      Form.BxSettingFileSelect.Click += EventButtonSettingFileSelect;
      Form.BxSettingFolderSelect.Click += EventButtonSettingFolderSelect;
      Form.BxSettingColorSelect.Click += EventButtonSettingColorSelect;
      Form.BxSettingFontSelect.Click += EventButtonSettingFontSelect;
      Form.BxGridRefresh.Click += EventRefreshGridSettings;
      Form.BxCopyFolderPath.Click += EventCopyFolderPathToClipboard;

      Form.DxTypes.SelectedValueChanged += EventSettingTypeChanged; // Select TYPE of a NEW variable //

      Form.PvSettings.SelectedPageChanged += EventSettingsSelectedPageChanged; // Settings Change, Add, Rename, Delete //

      Form.TvFolders.SelectedNodeChanged += EventTreeviewSelectedNodeChanged; // SELECTED NODE CHANGED //
      Form.ScMain.SplitterMoved += EventScMainSplitterMoved;
      Form.GvSettings.SelectionChanged += EventGridSelectionChanged; // User has selected a new row //
    }

    private async void EventCopyFolderPathToClipboard(object sender, EventArgs e)
    {
      Form.BxCopyFolderPath.Visibility = ElementVisibility.Collapsed;
      Clipboard.SetText(Form.TxFolderFullPath.Text);
      await Task.Delay(250);
      Form.BxCopyFolderPath.Visibility = ElementVisibility.Visible;
    }

    private void UserCanChangeDatabase(bool Allow)
    {
      Form.BxSelectFile.Enabled = Allow;
      Form.BxSelectFile.Visibility = Allow ? ElementVisibility.Visible : ElementVisibility.Collapsed;
    }

    private void ResetView()
    {
      ResetDataSourceForTreeview();
      VxGridSettings.ResetView();
      CurrentIdFolder = IdFolderNoValue;
      NameOfSelectedNode = string.Empty;
      CurrentIdSetting = string.Empty;
      CurrentSetting = null;
      SearchResult = null;
      SearchIterator = 0;
      Form.PvSettings.Visible = false;
    }

    private void ResetDataSourceForTreeview() => Form.TvFolders.DataSource = null;

    private void SetDatabaseFile(string PathToDatabaseFile)
    {
      ResetView();
    }

    private void SetPropertiesDateTimePicker()
    {
      Form.StxDatetime.DateTimePickerElement.ShowTimePicker = true;
      Form.StxDatetime.Format = DateTimePickerFormat.Custom;
      Form.StxDatetime.CustomFormat = Manager.CvManager.CvDatetime.DatetimeFormat;
    }

    private void EventForAllPageViewSelectedPageChanging(object sender, RadPageViewCancelEventArgs e)
    {
      e.Cancel = !FlagAllowChangeSelectedFolder;
    }

    private void ShowNotification(bool Success, string Message)
    {
      if (Message.Length < 1)
      {
        Form.PicSettingMessage.Image = null; Form.LxSettingMessage.Text = string.Empty;
      }
      else
      {
        if (Success)
        {
          Form.PicSettingMessage.Image = Form.PicOk.Image; Form.LxSettingMessage.ForeColor = Color.DarkGreen;
        }
        else
        {
          Form.PicSettingMessage.Image = Form.PicError.Image; Form.LxSettingMessage.ForeColor = Color.DarkViolet;
        }
        Form.LxSettingMessage.Text = Message;
      }
      Form.PvSettings.SelectedPage = Form.PgSettingMessage;
      Form.PgSettingMessage.Select(); // <-- If we do not do that the row of the grid remains selected //
    }

    private void EventButtonSettingFontSelect(object sender, EventArgs e)
    {
      FontDialog dialog = new FontDialog();
      DialogResult result = dialog.ShowDialog();
      if (result == DialogResult.OK) Form.StxFont.Text = Manager.CvManager.CvFont.ToString(dialog.Font);
    }

    private void EventButtonSettingColorSelect(object sender, EventArgs e)
    {
      RadColorDialog dialog = new RadColorDialog();
      if ((Form.StxColor.Text.Length > 0) && (Form.PvSettings.SelectedPage == Form.PgSettingChange)) dialog.SelectedColor = Manager.CvManager.CvColor.FromString(Form.StxColor.Text).Value;
      DialogResult result = dialog.ShowDialog();
      if (result == DialogResult.OK) Form.StxColor.Text = Manager.CvManager.CvColor.ToString(dialog.SelectedColor);
    }

    private void EventButtonSettingFolderSelect(object sender, EventArgs e)
    {
      RadOpenFolderDialog dialog = new RadOpenFolderDialog();
      DialogResult result = dialog.ShowDialog();
      if (result == DialogResult.OK) Form.StxFolder.Text = dialog.FileName;
    }

    private void EventButtonSettingFileSelect(object sender, EventArgs e)
    {
      RadOpenFileDialog dialog = new RadOpenFileDialog();
      DialogResult result = dialog.ShowDialog();
      if (result == DialogResult.OK) Form.StxFile.Text = dialog.FileName;
    }

    private void EventSettingsSelectedPageChanged(object sender, EventArgs e)
    {
      if ((Form.PvSettings.SelectedPage == Form.PgSettingAdd) && (Form.PvEditor.Parent != Form.PnSettingAddTool))
      {
        PanelSettingsChangeSizeBySettingType(TypeSetting.Unknown);
        Form.PvEditor.Parent = Form.PnSettingAddTool;
      }
      Form.DxTypes.SelectedIndex = (int)(TypeSetting.Unknown); // Unknown //
      SettingEditorResetAllInputControls();
    }

    private void SettingEditorResetAllInputControls()
    {
      Form.StxBoolean.Value = false;
      Form.StxDatetime.Value = DateTime.Today;
      Form.StxFile.Clear();
      Form.StxFolder.Clear();
      Form.StxLongInteger.Clear();
      Form.StxPassword.Clear();
      Form.StxText.Clear();
      Form.StxFont.Clear();
      Form.StxColor.Clear();
    }

    private int GetMessageBoxWidth(string message) => Math.Min(message.Length * 9, 500);

    internal TypeSetting GetTypeFromDropDownList()
    {
      int integerValue = 0;
      try { integerValue = Form.DxTypes.ZzGetIntegerValue(); } catch { integerValue = -1; };
      if (integerValue < 0) return TypeSetting.Unknown;
      return TypeSettingConverter.FromInteger(integerValue);
    }

    private void EventScMainSplitterMoved(object sender, SplitterEventArgs e)
    {
      if (Form.PnTreeview.SizeInfo.AbsoluteSize.Width > (2 * Form.PnUpper.Width) / 3)
        Form.PnTreeview.SizeInfo.AbsoluteSize = new Size((39 * Form.PnUpper.Width) / 100, 0);
    }

    private void EventTreeviewSelectedNodeChanged(object sender, RadTreeViewEventArgs e)
    {
      RequestToDb.RequestTreeviewSelectedNodeChanged(sender, e);
    }

    private void EventRefreshGridSettings(object sender, EventArgs e)
    {

    }

    private void EventSettingOneRowSelected() // Event - user selected a setting in the grid //
    {

    }

    private void EventButtonSettingRename(object sender, EventArgs e)
    {

    }

    private void EventButtonSettingDelete(object sender, EventArgs e)
    {

    }

    private void RefreshGridSettings()
    {

    }

    private void EventSettingClearSelection()
    {
      
    }

    private void RefreshGridSettingsAndClearSelection()
    {

    }

    private void EventGridSelectionChanged(object sender, EventArgs e)
    {

    }

    private void PutValueOfCurrentSettingToInputControl()
    {
 
    }

    private void EventButtonSettingChange(object sender, EventArgs e)
    {
 
    }

    private void EventButtonSettingCancel(object sender, EventArgs e)
    {

    }

    private void EventButtonSettingCancel()
    {

    }

    private void SelectOneNode(RadTreeNode node)
    {

    }

    private void EventButtonAddFolder(object sender, EventArgs e)
    {

    }

    private void EventButtonRenameFolder(object sender, EventArgs e)
    {
 
    }

    private void EventButtonDeleteFolder(object sender, EventArgs e)
    {

    }

    private void EventButtonSearchFolder(object sender, EventArgs e)
    {
 
    }

    private void EventButtonSearchFolderGotoNext(object sender, EventArgs e)
    {

    }

    private void EventButtonChooseFile(object sender, EventArgs e)
    {

    }

    private void EventLoadData(object sender, EventArgs e)
    {
      RequestToDb.RequestLoadData();
    }

    private void EventLoadDataFromDatabaseFile(bool LoadDataFirstTimeFromThisFile)
    {
 
    }

    private void EventLoadDataFromFileFirstTime()
    {

    }

    private void EventButtonSettingAddNew(object sender, EventArgs e)
    {

    }

    private void EventButtonSettingUpdateExisting(object sender, EventArgs e)
    {

    }

    private void EventSettingSave(bool AddNewSetting)
    {

    }

    private void PanelSettingsChangeSizeBySettingType(TypeSetting type)
    {
      
    }

    private void EventSettingTypeChanged(object sender, EventArgs e)
    {
      PanelSettingsChangeSizeBySettingType(GetTypeFromDropDownList());
    }

    private void SettingChangeRank(bool GotoUp)
    {

    }

    private void EventButtonSettingDown(object sender, EventArgs e)
    {

    }

    private void EventButtonSettingUp(object sender, EventArgs e)
    {

    }

    private void EventButtonNewFileName(object sender, EventArgs e)
    {

    }

    private void EventButtonCreateNewDatabase(object sender, EventArgs e)
    {

    }

    internal void EventPageChanged(string name)
    {

    }

    private void EventUserVisitsFormTreeViewPage() 
    {
      
    }

    private void EventUserLeavesFormTreeViewPage()
    {
    }

    public void EventEndWork()
    {
      Manager.Settings.TreeViewSize = Form.PnTreeview.SizeInfo.AbsoluteSize;
    }
  }
}
