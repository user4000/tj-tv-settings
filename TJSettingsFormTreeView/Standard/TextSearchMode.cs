﻿namespace TJSettingsFormTreeView
{
  public enum TextSearchMode
  {
    StartWith = 0,
    Contains = 1,
    WholeWord = 2
  }
}
