﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Akka.Actor;
using TJSettings;
using TJStandard;

namespace TJSettingsManagement
{
  public class AcDbOperator : ReceiveActor
  {
    LocalDatabaseOfSettings DbOperator { get; set; }

    List<Folder> Folders { get; set; } = null;

    Folder RootFolder { get; set; } = null;

    IActorRef ActorRootFolder { get; set; } = null;

    public AcDbOperator()
    {
      Receive<LocalDatabaseOfSettings>(arg => EventLocalDatabaseHasBeenOpened(arg));
      Receive<AskFolderActorConfigure>(arg => EventAskFolderActorConfigure(arg));
      Receive<AskTvRootNode>(arg => EventAskTvRootNode(arg));
      Receive<AnsTvRootNode>(arg => EventAnsTvRootNode(arg));
      Receive<AskFolderInfo>(arg => EventAskFolderInfo(arg));
      Receive<AnsFolderInfo>(arg => EventAnsFolderInfo(arg));
    }

    void EventLocalDatabaseHasBeenOpened(LocalDatabaseOfSettings arg)
    {
      DbOperator = arg;
      Folders = DbOperator.GetListOfFolders();
      //foreach (var item in Folders) Trace.WriteLine("----------------------- " + item.ToString());
      foreach (var item in Folders)
        if ((item.IdFolder == 0) && (item.Level == 0))
        {
          RootFolder = item;
          ActorRootFolder = Context.ActorOf<AcFolder>(RootFolder.IdFolder.ToString());
          ActorRootFolder.Tell(AskFolderActorConfigure.Create(Self, RootFolder));
          //Trace.WriteLine("Root folder ============ " + RootFolder.Path);
          break;          
        }
    }

    void EventAskFolderActorConfigure(AskFolderActorConfigure arg)
    {
      //Trace.WriteLine($">>>>>>>>>>>>>>>>>> DbOperator: Получен запрос AskFolderActorConfigure от актора {Sender.Path}");

      int count = Folders.Count(f => (f.IdParent == arg.Folder.IdFolder) && (f.Level == arg.Folder.Level + 1) );

      List<Folder> list = 
        (count > 0) 
        ?
        Folders.Where(f => (f.IdParent == arg.Folder.IdFolder) && (f.Level == arg.Folder.Level + 1)).ToList() 
        : 
        new List<Folder>();

      //Trace.WriteLine($">>>>>>>>>>>>>>>>>> DbOperator: response ");

      //foreach (var item in list) Trace.WriteLine("------ " + item.ToString());

      Sender.Tell(AnsFolderActorConfigure.Create(list));
    }

    void EventAskTvRootNode(AskTvRootNode arg)
    {
      ActorRootFolder.Tell(AskTvRootNode.Create());
    }

    void EventAnsTvRootNode(AnsTvRootNode arg)
    {
      Context.Parent.Tell(arg);
    }

    void EventAskFolderInfo(AskFolderInfo arg)
    {
      ActorRootFolder.Tell(arg);
    }

    void EventAnsFolderInfo(AnsFolderInfo arg)
    {
      Context.Parent.Tell(arg);
    }
  }
}